package pl.edu.pwr.wiz.laboratorium4;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    int MY_PERMISSIONS_REQUEST_PHONE =1;
private  String number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        final Long id = getIntent().getLongExtra("selectedId",0);

        CRMDbAdapter dbHelper = new CRMDbAdapter(this);
        dbHelper.open();

        Cursor c = dbHelper.fetchClientById(id);
        TextView name = findViewById(R.id.name_tv);
        name.setText(c.getString(c.getColumnIndex(CRMDbAdapter.Klienci.COLUMN_NAME_NAZWA)));
        final TextView address = findViewById(R.id.address_tv);
        address.setText(c.getString(c.getColumnIndex(CRMDbAdapter.Klienci.COLUMN_NAME_ADRES)));
        TextView tel = findViewById(R.id.telefon_tv);
        number = c.getString(c.getColumnIndex(CRMDbAdapter.Klienci.COLUMN_NAME_TELEFON));
        tel.setText(number);
        final MapView mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Geocoder geocoder = new Geocoder(DetailActivity.this);
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocationName(address.getText().toString(), 1);
                    if (addresses.size() > 0) {
                        double latitude = addresses.get(0).getLatitude();
                        double longitude = addresses.get(0).getLongitude();
                        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                        googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude)));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 15));
                        mapView.onResume();
                    }
                } catch (IOException ex){
                    Toast.makeText(getApplicationContext(), ex.getStackTrace().toString(), Toast.LENGTH_SHORT).show();

                }

            }
        });

        ImageButton callBtn = findViewById(R.id.imageButton);
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNumber();
            }
        });
    }

    private boolean callNumber() {
        if (number == null || number=="")
            return false;
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_REQUEST_PHONE);

            return false;
        }

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
        startActivity(intent);

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_PHONE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Dostęp nadany, uruchamiamy ponownie zrobienie zdjęcia
                callNumber();
            } else {
                // Dostęp nie udany. Wyświetlamy Toasta
                Toast.makeText(getApplicationContext(), R.string.access_denied, Toast.LENGTH_LONG).show();
            }
        }
    }
}
